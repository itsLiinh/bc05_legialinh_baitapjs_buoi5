/**Bài 1 */
function inKetQua() {
  console.log("yes");

  var diemChuan = document.getElementById("txt-diem-chuan").value * 1;
  var diemMon1 = document.getElementById("txt-diem-mon-1").value * 1;
  var diemMon2 = document.getElementById("txt-diem-mon-2").value * 1;
  var diemMon3 = document.getElementById("txt-diem-mon-3").value * 1;

  var khuVuc = document.getElementById("area").value;
  if (area == "A") {
    khuVuc = 2;
  } else if (area == "B") {
    khuVuc = 1;
  } else if (area == "C") {
    khuVuc = 0.5;
  } else {
    khuVuc = 0;
  }

  var doiTuong = document.getElementById("target").value;
  if (target == "1") {
    doiTuong = 2.5;
  } else if (target == "2") {
    doiTuong = 1.5;
  } else if (target == "3") {
    doiTuong = 1;
  } else {
    doiTuong = 0;
  }

  var tong = diemMon1 + diemMon2 + diemMon3 + khuVuc + doiTuong;

  if (tong >= diemChuan && (diemMon1 || diemMon2 || diemMon3 > 0)) {
    ketQua.innerHTML = "Đậu";
  } else {
    ketQua.innerHTML = "Rớt";
  }
}

/**Bài 2
 */

function tinhTienDien() {
  console.log("yes");

  var name = document.getElementById("txt-ho-ten").value;
  var soKw = document.getElementById("txt-so-kw").value * 1;
  var tongTienDien = 0;

  if (soKw <= 50) {
    tongTienDien = 500 * soKw;
  } else if (50 < soKw <= 100) {
    tongTienDien = 500 * soKw + 650 * (soKw - 50);
  } else if (100 < soKw >= 200) {
    tongTienDien = 500 * soKw + 650 * (soKw - 50) + 850 * (soKw - 100);
  } else if (200 < soKw <= 350) {
    tongTienDien =
      500 * soKw + 650 * (soKw - 50) + 850 * (soKw - 100) + 1100 * (soKw - 200);
  } else if (soKw > 350) {
    tongTienDien =
      500 * soKw +
      650 * (soKw - 50) +
      850 * (soKw - 100) +
      1100 * (soKw - 200) +
      1300 * (soKw - 350);
  }

  document.getElementById("tongTienDien").innerHTML = `Tên: ${name} , 
  Tổng tiền điện: ${tongTienDien}`;
}

/**Bài 3 */
function tinhTienThue() {
  var fullName = document.getElementById("txt-full-ten").value;
  var soNguoiPhuThuoc = document.getElementById("txt-nguoi-phu-thuoc").value;
  var tongThuNhap = document.getElementById("txt-tong-thu-nhap-nam").value * 1;

  if (tongThuNhap <= 60e6) {
    thueSuat = 5 / 100;
  } else if (60e6 < tongThuNhap <= 120e6) {
    thueSuat = 10 / 100;
  } else if (120e6 < tongThuNhap <= 210e6) {
    thueSuat = 15 / 100;
  } else if (210e6 < tongThuNhap <= 384e6) {
    thueSuat = 20 / 100;
  } else if (384e6 < tongThuNhap <= 624e6) {
    thueSuat = 25 / 100;
  } else if (624e6 < tongThuNhap <= 960e6) {
    thueSuat = 30 / 100;
  } else if (tongThuNhap > 960e6) {
    thueSuat = 35 / 100;
  }

  soTienThuePhaiDong = tongThuNhap * thueSuat - soNguoiPhuThuoc * 1.6;
  document.getElementById("tienThue").innerHTML = soTienThuePhaiDong;
}

/**Bài 4: Tính tiền cáp */
function chonLoaiKhachHang() {
  var loaiKhachHang = document.getElementById("customer").value;

  switch (loaiKhachHang) {
    case "nhaDan":
      soKetNoi.classList.remove("d-block");
      return 0;

    case "doanhNghiep":
      soKetNoi.classList.add("d-block");
      return 1;
    default:
  }
}

function tienCap() {
  var maKhachHang = document.getElementById("maKhachHang").value * 1;
  var soKenhCaoCap = document.getElementById("soKenhCaoCap").value * 1;
  var soKetNoi = document.getElementById("soKetNoi").value * 1;

  var tienCapNhaDan = 4.5 + 20.5 + 7.5 * soKenhCaoCap;
  // console.log("tienCapNhaDan: ", tienCapNhaDan);

  // Hàm để tính tiền cáp doanh nghiệp
  var tinhTienCapDoanhNghiep = function () {
    var tienCapDoanhNghiep = 15 + 75 + 50 * soKenhCaoCap;
    var tren10KetNoi = 5 * (soKetNoi - 10);

    if (soKetNoi < 10) {
      return tienCapDoanhNghiep;
    } else {
      return tienCapDoanhNghiep + tren10KetNoi;
    }
  };
  // console.log('tinhTienCapDoanhNghiep: ', tinhTienCapDoanhNghiep);

  var layGiaTri = function () {
    switch (chonLoaiKhachHang()) {
      case 0:
        return tienCapNhaDan;
      case 1:
        return tinhTienCapDoanhNghiep();
    }
  };

  var hoaDon = layGiaTri();

  document.getElementById(
    "tinhTien"
  ).innerHTML = ` Mã khách hàng: ; Tiền cáp: $${hoaDon}`;
}

